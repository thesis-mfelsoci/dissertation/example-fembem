# example-fembem

[![SWH](https://archive.softwareheritage.org/badge/origin/https://gitlab.inria.fr/thesis-mfelsoci/dissertation/example-fembem.git/)](https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.inria.fr/thesis-mfelsoci/dissertation/example-fembem.git)
[![SWH](https://archive.softwareheritage.org/badge/swh:1:dir:2f1ff4ffd940332c2e7480146ddb67d24be82cd2/)](https://archive.softwareheritage.org/swh:1:dir:2f1ff4ffd940332c2e7480146ddb67d24be82cd2;origin=https://gitlab.inria.fr/thesis-mfelsoci/dissertation/example-fembem.git;visit=swh:1:snp:32a3c2dc1d7790103919355e603342d9e5192fad;anchor=swh:1:rev:01396c9149c59062eec2aeb0e5c21ea3b16824a2)

This is an example of an experimental study based on the open-source version of
the `test_FEMBEM` solver test suite [3] and using the GNU Guix [1] transactional
package manager and literate programming [2] for better reproducibility.

Refer to the `study.org` file for the original study and to
`reproducing-guidelines.org` to learn how to reproduce the study including all
the numerical experiments.

## References

1. GNU Guix software distribution and transactional package manager
   [https://guix.gnu.org](https://guix.gnu.org).
2. Literate Programming, Donald E. Knuth, 1984
   [https://doi.org/10.1093/comjnl/27.2.97](https://doi.org/10.1093/comjnl/27.2.97).
3. test_FEMBEM, a simple application for testing dense and sparse solvers with
   pseudo-FEM or pseudo-BEM matrices
   [https://gitlab.inria.fr/solverstack/test_fembem](https://gitlab.inria.fr/solverstack/test_fembem).
