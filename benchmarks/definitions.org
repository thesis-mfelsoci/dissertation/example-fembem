* Defining benchmarks
:PROPERTIES:
:CUSTOM_ID: example-definitions
:END:

Within the experimental part of the study, we want to run the benchmark cases
specified in the comma-separated values ~definitions.csv~ file in Listing
[[listing:example-definitions]]. The latter features six columns:

1. =system_type= indicating the type of linear system (=fem=, =bem= or
   =fembem=),
2. =solver= specifying the solver to use (=hmat= or =chameleon=),
3. =threads= giving the number of threads to use for computation,
4. =nbpts= giving the number of unknowns in the system, i.e. the size of the
   system,
5. =arith= defining the arithmetic and precision of matrix coefficients (=s= for
   simple real, =d= for double real, =c= for simple complex or =z= for double
   complex),
6. =sym= indicating the symmetry of the coefficient matrix (=1= for symmetric or
   =0= for non-symmetric).

#+CAPTION: ~definitions.csv~ file defining benchmark cases to run within the
#+CAPTION: experimental part of the study.
#+NAME: listing:example-definitions
#+HEADER: :tangle definitions.csv :eval no
#+BEGIN_SRC text
system_type,solver,threads,nbpts,arith,sym
fembem,hmat,1,1000,z,1
fembem,hmat,1,2000,z,1
fembem,hmat,1,4000,z,1
fembem,hmat,1,6000,z,1
fembem,hmat,1,8000,z,1
fembem,chameleon,1,1000,z,1
fembem,chameleon,1,2000,z,1
fembem,chameleon,1,4000,z,1
fembem,chameleon,1,6000,z,1
fembem,chameleon,1,8000,z,1
fembem,chameleon,4,1000,z,1
fembem,chameleon,4,2000,z,1
fembem,chameleon,4,4000,z,1
fembem,chameleon,4,6000,z,1
fembem,chameleon,4,8000,z,1
#+END_SRC

We then pass the ~definitions.csv~ file to the ~run.sh~ shell script (see
Section [[#example-run]]) we use to execute all the benchmarks.
